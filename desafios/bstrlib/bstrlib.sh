#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Função 1: trim()
# -----------------------------------------------------------------------------
# Uso      : trim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do inínicio e do fim da string
#            e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
trim() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [CARACTERE|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n result=$2
  charlist=${3:-[:space:]}
  re="[^$charlist]"
  : "${1#"${1%%$re*}"}"
  result="${_%"${_##*$re}"}"
}
# -----------------------------------------------------------------------------
# Função 2: ltrim()
# -----------------------------------------------------------------------------
# Uso      : ltrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
ltrim() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [CARACTERE|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n result=$2
  charlist=${3:-[:space:]}
  re="[^$charlist]"
  result="${1#"${1%%$re*}"}"
}
# -----------------------------------------------------------------------------
# Função 3: rtrim()
# -----------------------------------------------------------------------------
# Uso      : rtrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do fim da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
rtrim() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [CARACTERE|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n result=$2
  charlist=${3:-[:space:]}
  re="[^$charlist]"
  result="${1%"${1##*$re}"}"
}
# -----------------------------------------------------------------------------
# Função 4: strim()
# -----------------------------------------------------------------------------
# Uso      : strim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início e do fim da string,
#            trunca ocorrências múltiplas do mesmo caractere
#            na string (sqeeze) e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strim() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [CARACTERE|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  local glob_status
  shopt -p extglob >/dev/null || { shopt -s extglob; glob_status=1; }
  declare -n result=$2
  re="[${3:-[:space:]}]"
  [[ $1 =~ $re ]]
  char=$BASH_REMATCH
  result="${1//+($re)/$char}"
  [[ $glob_status ]] && shopt -u extglob
  result="${result#$char}"
  result="${result%$char}"
}
# -----------------------------------------------------------------------------
# Função 5: squeeze()
# -----------------------------------------------------------------------------
# Uso      : squeeze 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Trunca para uma as ocorrências múltiplas do
#            mesmo caractere na string e atribui a uma
#            variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
squeeze() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [CARACTERE|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  local glob_status
  shopt -p extglob >/dev/null || { shopt -s extglob; glob_status=1; }
  declare -n result=$2
  re="[${3:-[:space:]}]"
  [[ $1 =~ $re ]]
  char=$BASH_REMATCH
  result="${1//+($re)/$char}"
  [[ $glob_status ]] && shopt -u extglob
  return $return_code
}
# -----------------------------------------------------------------------------
# Função 6: strlen()
# -----------------------------------------------------------------------------
# Uso      : strlen 'VAR'
# Descrição: Imprime o número de caracteres da string em VAR.
# Saída    : O número de caracteres da string.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strlen() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]}"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  : "${!1}"
  echo ${#_}
}
# -----------------------------------------------------------------------------
# Função 7: explode()
# -----------------------------------------------------------------------------
# Uso      : explode 'STRING' 'ARRAY' ['SEPARADOR|CLASSE']
# Descrição: Quebra a string nas ocorrências de um separador
#            e atribui cada parte a um elemento de um vetor.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
explode() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=STRING [2]=ARRAY)
  usage="  Usage: $FUNCNAME ${required[@]} [SEPARADOR|CLASSE]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  IFS='|' read -a $2 <<< "${1//[${3:-,}]/|}"
}
# -----------------------------------------------------------------------------
# Função 8: implode()
# -----------------------------------------------------------------------------
# Uso      : implode 'VETOR' 'VAR' ['SEPARADOR']
# Descrição: Une os elementos de um vetor em uma string
#            intercalados por um separador e atribui
#            a uma variável.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
implode() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=VETOR [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]} [SEPARADOR]"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n array=$1 result=$2
  local sep=${3:-,}
  printf -v result "%s$sep" "${array[@]}"
  result=${result%$sep}
}
# -----------------------------------------------------------------------------
# Função 9: in_array()
# -----------------------------------------------------------------------------
# Uso      : in_array 'STRING|PADRÃO' 'VETOR'
# Descrição: Verifica se uma string ou padrão (FNPM)
#            acontece em algum dos elementos do vetor.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global ARRAY_MATCHES com os
#            índices dos elementos onde houve casamento.
#            Sai com sucesso se a string for encontrada.
#            Sai com erro 1 se a string não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_array() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]='STRING|PADRÃO' [2]=VETOR)
  usage="  Usage: $FUNCNAME ${required[@]}"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=2
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n array=$2
  ARRAY_MATCHES=()
  for i in "${!array[@]}"; do
    [[ ${array[i]} =~ $1 ]] && ARRAY_MATCHES+=($i)
  done
  ((${#ARRAY_MATCHES[@]})) || return_code=1
  return $return_code
}
# -----------------------------------------------------------------------------
# Função 10: in_string()
# -----------------------------------------------------------------------------
# Uso      : in_string 'STRING|PADRÃO' 'VAR'
# Descrição: Verifica se uma substring ou padrão (FNPM)
#            acontece em algum ponto da string em VAR.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global STRING_POS com os
#            índices dos caracteres na string a partir dos
#            quais houve casamento.
#            Sai com sucesso se a substring for encontrada.
#            Sai com erro 1 se a substring não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_string() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]='STRING|PADRÃO' [2]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]}"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=2
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  local offset
  STRING_POS=()
  substr=${!2}
  while [[ $substr =~ $1 ]]; do
    : "${substr%%$BASH_REMATCH*}"
    STRING_POS+=($((${#_} + offset)))
    substr="${substr#*$BASH_REMATCH}"
    offset=$((${#BASH_REMATCH} + ${STRING_POS[-1]}))
  done
  ((${#STRING_POS[@]})) || return_code=1
  return $return_code
}
# -----------------------------------------------------------------------------
# Função 11: str_repeat()
# -----------------------------------------------------------------------------
# Uso      : str_repeat 'STRING|CHAR' REPETIÇÕES 'VAR'
# Descrição: Define uma variável associada ao resultado das
#            repetições de um caractere ou de uma string.
# Saída    : Não produz dados na saída padrão.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
str_repeat() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]='STRING|CHAR' [2]=REPETIÇÕES [3]=VAR)
  usage="  Usage: $FUNCNAME ${required[@]}"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=1
      echo "Param ${required[param]} is required"
    }
  done
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n str=$3
  printf -v str '%*s' $2
  str=${str// /$1}
}
# -----------------------------------------------------------------------------
# Função 12: count()
# -----------------------------------------------------------------------------
# Uso      : count 'VETOR'
# Descrição: Define a variável global ARRAY_COUNT com
#            a quantidade de elementos de um vetor.
# Saída    : Não produz dados na saída padrão.
#            Sai com erro 1 se a variável não for um
#            vetor ou com erro 2 se houver erro de
#            parâmetros.
count() {
# validation ------------------------------------------------------------------
  local return_code=0
  required=([1]=VETOR)
  usage="  Usage: $FUNCNAME ${required[@]}"
  for param in ${!required[@]}; do
    [[ ${!param} ]] || {
      return_code=2
      echo "Param ${required[param]} is required"
    }
  done
  [[ $(declare -p $1) =~ ^declare\ -[a|A] ]] || return_code=1
  [[ $return_code -ne 0 ]] && { echo "$usage"; return $return_code; }
# main ------------------------------------------------------------------------
  declare -n array=$1
  ARRAY_COUNT=${#array[@]}
}
