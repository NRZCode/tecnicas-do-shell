# TÉCNICAS DO SHELL #1
# O desafio do terapeuta quântico (ELIZA)

+ O terapeuta quântico

* Parte 2 - Técnicas envolvidas (modelagem)

* Loop de conversas aleatórias

- Ciclos de leitura, avaliação e exibição (REPL)
- Leitura da entrada padrão
- Estruturas de repetição

* Respostas pré-determinadas aleatórias

- Seleção aleatória de dados ('RANDOM' e 'shuf')

* Respostas de acordo com a entonação

- Casamento de padrões de texto (PM e REGEX)
