# TÉCNICAS DO SHELL #1
# O desafio do terapeuta quântico (ELIZA)

+ O terapeuta quântico

* Parte 1 - O problema (descrição)

Construir um loop de conversas aleatórias com um
terapeuta artificial (TA) que responde de acordo
com a entonação do utilizador.

* Pontos-chave:

- Loop de conversas aleatórias
- Respostas pré-determinadas aleatórias
- Respostas de acordo com a entonação
