#!/usr/bin/env bash

# requisitos do desafio ------------------------------------------------------
#
# - O script deverá conter três funções, uma para cada método de arredondamento.
# - O tipo de arredondamento a ser aplicado será definido na linha do comando.
# - As três funções devem ser independentes e reaproveitáveis em outros scripts.
# - Utilizar apenas comandos internos do Bash.
# - Possíveis erros de invocação do script devem ser tratados.
#
# Uso: round.sh [round(default)|ceil|floor] NÚMERO
#      round.sh -h|--help
# ----------------------------------------------------------------------------

default=round

# strings --------------------------------------------------------------------

version='0.1'

err[1]=$'\e[33;1mNúmero incorreto de argumentos\e[m\n'
err[2]=$'\e[33;1mArgumentos inválidos\e[m\n'

msg_header="Arredondar para inteiros (round) $version"

usage="USO

    $0 [COMANDO] NÚMERO
    $0 OPÇÃO

DESCRIÇÃO

    Arredonda números decimais para inteiros de acordo com
    o comando utilizado. Se COMANDO não for fornecido, será
    utilizado o comando \e[1m\`$default\e[m'

COMANDOS

    \e[1mround\e[m - NÚMERO é arredondado para o inteiro mais
            próximo, seja ele maior ou menor.

    \e[1mceil\e[m  - NÚMERO é arredondado para o maior valor
            inteiro seguinte.

    \e[1mfloor\e[m - NÚMERO é arredondado para o menor valor
            inteiro anterior.

OPÇÕES

    -h --help    - Exibe esta ajuda.
    -v --version - Exibe versão e licença.

"

copyright="$msg_header

Copyright (C) 2022 Blau Araujo <blau@debxp.org>.
Licença GPLv3+: GNU GPL versão 3 ou posterior <https://gnu.org/licenses/gpl.html>
Este é um software livre: você é livre para alterá-lo e redistribuí-lo.
NÃO HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei.

Escrito por Blau Araujo.
"

# funções do desafio ---------------------------------------------------------

# Round (arredondar)
# O valor é arredondado para o inteiro mais próximo, seja ele maior ou menor.
round() {
    local sig int dec sum
    [[ $1 == *[.,]*  ]] || { echo $1; return; }
    [[ ${1::1} = '-' ]] && sig=-
    int=${1%[.,]*}
    dec=${1#*[.,]}
    [[ ${dec::1} -ge 5 ]] && sum=${sig}1
    echo $((int + sum))
}

# Ceil (teto)
# O valor é arredondado para o maior valor inteiro seguinte.
ceil() {
    local sig int dec sum
    [[ $1 == *[.,]*  ]] || { echo $1; return; }
    [[ ${1::1} = '-' ]] && sig=-
    int=${1%[.,]*}
    dec=${1#*[.,]}
    [[ -z $sig && $dec -gt 0 ]] && sum=1
    echo $((int + sum))
}

# Floor (piso)
# O valor é arredondado para o menor valor inteiro anterior.
floor() {
    local sig int dec sum
    [[ $1 == *[.,]*  ]] || { echo $1; return; }
    [[ ${1::1} = '-' ]] && sig=-
    int=${1%[.,]*}
    dec=${1#*[.,]}
    [[ $sig && $dec -gt 0 ]] && sum='-1'
    echo $((int + sum))
}

# Validação
check_args() {
    local commands options
    commands='round,ceil,floor'
    options='-h,--help,-v,--version'
    case $# in
        1)  # Argumentos inválidos...
            is_number $1 || in_string $1 "$options" || die 2
            ;;
        2)  # Argumentos inválidos...
            in_string $1 "$commands" && is_number $2 || die 2
            ;;
        *)  # Número incorreto de argumentos...
            die 1
            ;;
    esac
}

# Ajuda
help() {
    printf "$usage"
}

# Versão
version() {
    printf "$copyright"
}

# funções auxiliares ---------------------------------------------------------

# Usa regex para validar o número
is_number() [[ "$1" =~ ^-?[0-9]+[.,]?[0-9]*$ ]]

# Verifica se a string buscada existe em outra string
in_string() [[ "$2" == *$1* ]]

# Sai imprimindo uma mensagem de erro
die() {
    echo "${err[$1]}"
    help
    exit $1
}

# fluxo principal (main) -----------------------------------------------------

# Validação de argumentos...
check_args "$@"

# Executa o comando recebido...
case $1 in
    -h|--help) help;;
    -v|--version) version;;
    round|ceil|floor) $1 $2;;
    *) $default $1
esac


