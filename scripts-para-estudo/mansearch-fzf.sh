#!/usr/bin/env bash

fo='--reverse -e -i --tiebreak=begin --prompt=Manual: --preview-window=down:65%'

clear

page=$(man -k . | fzf $fo --preview='p={}; n=${p##*(}; man ${n%%)*} ${p%% *} 2> /dev/null')

if [[ -n $page ]]; then
    p=${page%% *}
    m=${page##*(}
    man ${m%%)*} $p
fi
