#!/usr/bin/env bash

# Jogadores -----------------------------------------------------

# Marcas
mark=('X' 'O')

player_menu='
  Jogo da velha

  Contra quem você quer jogar?

  1) Jogador robótico
  2) Jogador humano
'

# Alterna jogador
change_player() { (($cp)); cp=$?; }

# Menu de seleção
select_mode() {
    while :; do
        clear
        echo "$player_menu"
        read -sn1 -p '  Tecle a sua opção: '
        # Rótulos...
        case $REPLY in
            1) player=('robótico' 'humano')
               mode=0
               break
               ;;
            2) player=('1' '2')
               mode=1
               break
               ;;
        esac
    done
    # Primeiro a jogar...
    (($mode)) && cp=0 || cp=1
}

# Tabuleiro -----------------------------------------------------

# Tabuleiro descrito como array
#       0  1  2  3  4  5  6  7  8
board=(-1 -1 -1 -1 -1 -1 -1 -1 -1)

# Tabuleiro como coordenadas
#       0  1  2  3  4  5  6  7  8
coord=(A1 A2 A3 B1 B2 B3 C1 C2 C3)

# Tabuleiro como grade
grid="
    Jogo da Velha

      1   2   3
    +---+---+---+
  A |   |   |   |
    +---+---+---+
  B |   |   |   |
    +---+---+---+
  C |   |   |   |
    +---+---+---+
"

# Tabuleiro como células
cell=('5 6' '5 10' '5 14' '7 6' '7 10' '7 14' '9 6' '9 10' '9 14') 

# Coordenadas do prompt
prompt_pos='12 2'

# Coordenadas das mensagens de vitória e empate
msg_pos='14 2'

# Textos do prompt
prompt_move='Jogador %s: '
msg_win='O jogador %s venceu!\n\n'
msg_tie='O jogo empatou!'
msg_cont='Tecle algo para continuar... '
declare -A msg_err

msg_err[0]='\033[0;31mJogada inválida!\033[0m'
msg_err[1]="${msg_err[0]}\n\n  A célula já está ocupada!"
msg_err[2]="${msg_err[0]}\n\n  A coordenada fora dos limites!"

# Exibe o tabuleiro
print_board() {
    clear
    echo -e "$grid"
}

# Exibe a marca do jogador
print_mark() {
    tput cup $2
    echo -e $1
    tput cup $prompt_pos
}

# Exibe o prompt
show_prompt() {
    unset player_move
    tput cup $prompt_pos
    tput ed
    printf "$prompt_move" ${player[$cp]}
    read
    player_move=${REPLY^^}
}

# Exibe mensagens de erro
show_error() {
    tput cup $msg_pos
    tput ed
    echo -e "${msg_err[$1]}"
    read -sn1 -p "  $msg_cont"
}

show_tie() {
    tput cup $msg_pos
    tput ed
    echo -e "$msg_tie\n"
}

# Registro de jogadas -------------------------------------------

in_array() {
    local i
    local arr=(${@:2})
    local val=${@:1:1}
    for i in ${!arr[@]}; do
        [[ $val == ${arr[$i]} ]] && return 0
    done
    # Retorna erro se não encontrar...
    return 1
}

get_index() {
    local i
    local arr=(${@:2})
    local val=${@:1:1}
    for i in "${!arr[@]}"; do
        if [[ $val == ${arr[$i]} ]]; then
            echo $i
            break
        fi
    done
}

# Registra a joga e checa por erros
reg_move() {
    local move
    # Verifica se a jogada está nos limites das coordenadas...
    if in_array $player_move ${coord[@]}; then
        # Pega o índice das coordenadas...
        move=$(get_index $player_move ${coord[@]})
        # Verifica se a célula está ocupada...
        if [[ ${board[$move]} -lt 0 ]]; then
            # Registra o número do jogador no tabuleiro...
            board[$move]=$cp
            # Desenha a marca do jogador na grade...
            print_mark ${mark[$cp]} "${cell[$move]}"
            return 0
        else
            # Célula ocupada...
            show_error 1
            unset $player_move
            return 1
        fi
    else
        # Fora das coordenadas...
        show_error 2
        unset $player_move
        return 1
    fi
}

# Condições de vitória ------------------------------------------

# tabuleiro 0 1 2, 3 4 5, 6 7 8 (linhas horizontais)
win_row() {
    # Faz as contagens dos inícios das linhas....
    for ((i=0; i<=8; i=i+3)); do
        r1=${board[$i]}
        # Verifica se primeira posição da linha está ocupada...
        if [[ $r1 -ne -1 ]]; then
            # Pega os valores no restante da linha...
            r2=${board[$i+1]}
            r3=${board[$i+2]}
            # Registra coordenadas para o caso de vitória...
            cwin=($i $((i+1)) $((i+2)))
            # Testa se linha está ocupada pelo mesmo jogador...
            if [[ $r1 -eq $r2 && $r2 -eq $r3 ]]; then
                printf "$msg_win" ${player[$r1]}
                return 0
            fi
        fi
    done
    return 1
}

# tabuleiro 0 3 6, 1 4 7, 2 5 8 (linhas verticais)
win_col() {
    # Faz as contagens dos inícios das colunas....
    for ((i=0; i<=2; i++)); do
        c1=${board[$i]}
        # Verifica se primeira posição da coluna está ocupada...
        if [[ $c1 -ne -1 ]]; then
            # Pega os valores no restante da coluna...
            c2=${board[$i+3]}
            c3=${board[$i+6]}
            # Registra coordenadas para o caso de vitória...
            cwin=($i $((i+3)) $((i+6)))
            # Testa se linha está ocupada pelo mesmo jogador...
            if [[ $c1 -eq $c2 && $c2 -eq $c3 ]]; then
                printf "$msg_win" ${player[$c1]}
                return 0
            fi
        fi
    done
    return 1
}

# tabuleiro 0 4 8, 2 4 6 (linhas diagonais)
win_diag() {
    # Testa a diagonal esquerda...
    if [[ ${board[0]} -ne -1 ]]; then
        d1=${board[0]}
        d2=${board[4]}
        d3=${board[8]}
        # Registra coordenadas para o caso de vitória...
        cwin=(0 4 8)
        if [[ $d1 -eq $d2 && $d2 -eq $d3 ]]; then
            printf "$msg_win" ${player[$d1]}
            return 0
        fi
    fi
    # Testa a diagonal direita...
    if [[ ${board[2]} -ne -1 ]]; then
        d1=${board[2]}
        d2=${board[4]}
        d3=${board[6]}
        # Registra coordenadas para o caso de vitória...
        cwin=(2 4 6)
        if [[ $d1 -eq $d2 && $d2 -eq $d3 ]]; then
            printf "$msg_win" ${player[$d1]}
            return 0
        fi
    fi
    return 1
}

# Verifica se houve vitória
check_win() {
    win_col && exit_win
    win_row && exit_win
    win_diag && exit_win
}

exit_win() {
    echo -e "\033[1;33m"
    for i in ${cwin[@]}; do
        tput cup ${cell[i]}
        echo -e ${mark[$cp]}
    done
    echo -e "\033[0m"
    tput cup 15 0
    exit
}

# Pseudo IA -----------------------------------------------------

pia() {
    local r

    unset player_move
    tput cup $prompt_pos
    tput ed
    printf "${prompt_move}pensando..." ${player[$cp]}

    while :; do
        # Gerar número aleatório entre 0 e 8...
        r=$(( $RANDOM % 9 ))
        # Checar se índice está ocupado...
        if [[ ${board[$r]} -eq -1 ]]; then
            player_move=${coord[$r]}
            break
        fi
    done
    sleep 2
    tput ed
}

# Função principal ----------------------------------------------

_main() {

    # Seleção de modo
    select_mode

    # Exibe a grade
    print_board

    turn=1

    while [[ $turn -le 9 ]]; do
        # Exibe o prompt ou espera jogada da IA...
        if (($mode)); then
            show_prompt
        else
            (($cp)) && show_prompt || pia
        fi
        # Registra a jogada e checa por erros...
        reg_move || continue
        # Testar vitórias...
        check_win
        # Troca jogador...
        change_player
        # Testa se há mais turnos...
        ((turn++))
    done
    show_tie
}

_main
